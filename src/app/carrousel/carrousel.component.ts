import { Component, OnInit, ViewChild } from '@angular/core';
import {CarouselService} from '../carousel.service';
import { NgbSlideEvent, NgbSlideEventSource, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

@Component({
  selector: 'app-carrousel',
  templateUrl: './carrousel.component.html',
  styleUrls: ['./carrousel.component.css']
})
export class CarrouselComponent implements OnInit {
  
  @ViewChild('mycarousel', {static : true}) carousel: NgbCarousel;
  constructor(protected carouselservice : CarouselService) { }
  fotosCarousel:any[]=[];
  


  ngOnInit() {
    
    //llama al servicio con las imagenes en el objeto
    this.carouselservice.fotos();

    //convertir objeto
    this.fotosCarousel =JSON.parse(localStorage.getItem('fotosCarousel'));
    console.log(this.fotosCarousel);
  }

  goToSlide(slide) {
    this.carousel.select(slide);
  }

}
